//
// ────────────────────────────────────────────── I ──────────
//   :::::: A P P : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────
//

//
// ─── CREATE SERVER ──────────────────────────────────────────────────────────────
//
  const express = require('express');
  const server = require('./server/server');
  const app = server(express());

//
// ─── INITIALIZE DATABASE ────────────────────────────────────────────────────────
//
  require('./config/database');

//
// ─── INITIALIZE SERVER ──────────────────────────────────────────────────────────
//
  app.listen(app.get('port'), () => {
    console.log(`Server on port: ${app.get('port')}`);
  })