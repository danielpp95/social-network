const mongoose = require('mongoose');

const { database } = require('./env/env');

const options = {
  useNewUrlParser: true
}

mongoose.connect(database.URI, options)
  .then(db => console.log('DB connected'))
  .catch(err => console.log(`ERROR: ${err}`));