const isProduction = false

isProduction ? 
  module.exports = require('./production')
:
  module.exports = require('./development')