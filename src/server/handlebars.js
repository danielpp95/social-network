//
// ────────────────────────────────────────────────────────────── III ─────────
//   :::::: V I E W   E N G I N E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────
//
const exphbs = require('express-handlebars');
const path = require('path');

module.exports = app => {

  app.set('views', path.join(__dirname, '../views'));
  app.engine('.hbs', exphbs({defaultLayout: 'main',
  layoutsDir: path.join(app.get('views'), 'layouts'),
  partialsDir: path.join(app.get('views'), 'partials'),
  extname: '.hbs'
  }));
  app.set('view engine', '.hbs');
  
  return app
}