//
// ──────────────────────────────────────────────────── IV ───────────
//   :::::: R O U T E S : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────
//

const router  = require('express').Router();


//
// ─── ROUTES ─────────────────────────────────────────────────────────────────────
/** 
 * @param method : 'GET' || 'POST' || 'PUT' || 'DELETE'
 * @param route: '/example'
 * @param controller: 'index/addElement'
*/
const routes = [
  {method: 'GET',     route: '/',                               controller: 'home/index'},
  {method: 'GET',     route: '/images/:image_id',               controller: 'image/getImage'},
  {method: 'POST',    route: '/images',                         controller: 'image/createImage'},
  {method: 'POST',    route: '/images/:image_id/like',          controller: 'image/like'},
  {method: 'POST',    route: '/images/:image_id/comment',       controller: 'image/createComment'},
  {method: 'DELETE',  route: '/images/:image_id',               controller: 'image/delete'},
]

//
// ─── EXPORT CONFIGURED ROUTES ───────────────────────────────────────────────────
//

routes.forEach(r => {
  const { method, route, controller } = r

  const fn = require('./fn')(controller)
  // const c = path.join(controllers, controller)

  switch (method) {
    case ('GET'):
      router.get(route, fn);
      break;
      
    case ('POST'):
      router.post(route, fn);
      break;

    case ('PUT'):
      router.get(route, fn);
      break;
      
    case ('DELETE'):
      router.get(route, fn);
      break;
  
    default:
      console.log(`ERROR into routes: {method: '${method}', route: '${route}', controller: '${controller}'}`)
      break;
  }
});

module.exports= router;