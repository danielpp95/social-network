const path = require('path');
const controllers = path.join(__dirname, '../controllers')



module.exports = controller => {
  var p = ''

  try {
    p = path.join(controllers, controller)
  } catch (error) {
    console.log(`Cannot join routes ${controllers}, ${controller}`);
  }
  
  var method;
  try {
    method = require(p).fn;
  } catch (error) {
    console.log(`ERROR: Cannot get file ${controller}`)
    // return throw
  }

  return method;

} 